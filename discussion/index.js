const http = require('http');

const port = 4000

const server = http.createServer(function(request, response) {
	// request.method is used to get the HTTP method that is currently used by the browser to send a request
	if(request.url == '/items' && request.method == 'GET') {
		// In this case, we are checking if the current request method is a 'GET HTTP' method
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Data is retrieved from the Database')
	} else if (request.url == '/items' && request.method == 'POST') {
		// In this case, we are checking if the current request method is a 'POST HTTP' method
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Data is sent to the Database')
	}
})

server.listen(port)

console.log(`Server is now accessible at localhost: ${port}`);

// Good practice to close tabs in postman if you're not using them.
